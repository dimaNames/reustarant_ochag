from django.contrib import admin
from .models import *
from django.forms import ModelChoiceField, ModelForm

class ProductImageAdmin(admin.ModelAdmin):
    pass


class MenuImageInline(admin.StackedInline):
    model = MenuImage
    max_num = 30
    extra = 0


class TypeMenuAdmin(admin.ModelAdmin):
    inlines = [MenuImageInline]

admin.autodiscover()
admin.site.enable_nav_sidebar = False


admin.site.register(Review)
admin.site.register(Category)
admin.site.register(CartProduct)
admin.site.register(Cart)
admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(Mainmenu)
admin.site.register(SecondMenu)
