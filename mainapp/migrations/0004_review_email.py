# Generated by Django 3.1.4 on 2021-01-04 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0003_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='email поле'),
        ),
    ]
